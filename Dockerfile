FROM google/cloud-sdk:290.0.1-alpine
COPY requirement.txt .
RUN apk --update add python3 ; pip3 install -r requirement.txt
COPY . .
USER nobody
ENTRYPOINT ["sh", "/start.sh"]
