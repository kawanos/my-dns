from flask import Flask, request, jsonify
from google.cloud import dns
import os, sys

app = Flask(__name__)

PROJECT_ID = os.environ.get('PROJECT_ID')
ZONE_ID = os.environ.get('ZONE_ID')
ZONE_NAME = os.environ.get('ZONE_NAME')
SECRET = os.environ.get('SECRET', 'shingo')
TIMEOUT = 60
CMD_TEXT = "cmd.txt"

class WrongAuth(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Wrong Authentication"

def _auth():
    if request.headers.getlist("X-My-Auth")[0] == SECRET:
        return True
    raise WrongAuth

@app.route('/update/<hostname>')
def _register(hostname):
    try:
        _auth()
    except WrongAuth as e:
        return jsonify({'result': str(e)}), 403

    full_hostname = hostname + '.' + ZONE_NAME
    ip = request.headers.getlist("X-Forwarded-For")[0]
    client = dns.Client(project=PROJECT_ID)
    zone = client.zone(ZONE_ID, ZONE_NAME)
    try:
        soa, *record_sets = zone.list_resource_record_sets()
        for record_set in record_sets:
            if record_set.name == full_hostname:
                changes = zone.changes()
                changes.delete_record_set(record_set)
                changes.create()
                break
    except Exception as e:
        print(str(e), file=sys.stderr)
    try:
        record_set = zone.resource_record_set(
            full_hostname,
            'A',
            TIMEOUT,
            [ip]
        )
        changes = zone.changes()
        changes.add_record_set(record_set)
        changes.create()
        changes.reload()
        result = {'hostname':full_hostname,'ip':ip}
    except Exception as e:
        print(str(e), file=sys.stderr)
        result = str(e)
    return jsonify({'result':result}), 200

@app.route('/test')
def _test():
    return "Revision: " + os.environ.get('K_REVISION')

@app.route('/environment')
def _environment():
    e = os.environ
    return str(os.environ.__dict__)

@app.route('/cmd')
def _text():
    with open(CMD_TEXT) as f:
        content = f.read()
    return content, 200, {'Content-Type': 'text/plain; charset=utf-8'}

if __name__ == "__main__":
    app.run(debug=True)
